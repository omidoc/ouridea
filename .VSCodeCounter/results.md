# Summary

Date : 2020-04-15 13:56:05

Directory /Users/omid_ch/Documents/Development/OurIdea

Total : 44 files,  19689 codes, 104 comments, 215 blanks, all 20008 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 7 | 17,732 | 0 | 5 | 17,737 |
| JavaScript | 34 | 1,906 | 81 | 207 | 2,194 |
| HTML | 1 | 30 | 23 | 1 | 54 |
| CSS | 2 | 21 | 0 | 2 | 23 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 44 | 19,689 | 104 | 215 | 20,008 |
| api | 3 | 275 | 34 | 49 | 358 |
| client | 32 | 16,853 | 60 | 141 | 17,054 |
| client/public | 2 | 55 | 23 | 2 | 80 |
| client/src | 28 | 1,499 | 37 | 137 | 1,673 |
| client/src/actions | 3 | 181 | 25 | 30 | 236 |
| client/src/components | 13 | 1,100 | 6 | 75 | 1,181 |
| client/src/components/homepage | 3 | 281 | 0 | 26 | 307 |
| client/src/components/profile | 3 | 215 | 0 | 14 | 229 |
| client/src/components/routing | 1 | 25 | 0 | 5 | 30 |
| client/src/components/subsite | 5 | 530 | 6 | 24 | 560 |
| client/src/reducers | 3 | 104 | 2 | 8 | 114 |
| client/src/utils | 2 | 15 | 0 | 2 | 17 |
| config | 3 | 31 | 1 | 4 | 36 |
| middleware | 1 | 18 | 3 | 4 | 25 |
| models | 2 | 98 | 0 | 5 | 103 |

[details](details.md)