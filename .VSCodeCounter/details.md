# Details

Date : 2020-04-15 13:56:04

Directory /Users/omid_ch/Documents/Development/OurIdea

Total : 44 files,  19689 codes, 104 comments, 215 blanks, all 20008 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [api/auth.js](/api/auth.js) | JavaScript | 68 | 8 | 15 | 91 |
| [api/team.js](/api/team.js) | JavaScript | 51 | 9 | 6 | 66 |
| [api/users.js](/api/users.js) | JavaScript | 156 | 17 | 28 | 201 |
| [client/package-lock.json](/client/package-lock.json) | JSON | 15,258 | 0 | 1 | 15,259 |
| [client/package.json](/client/package.json) | JSON | 41 | 0 | 1 | 42 |
| [client/public/index.html](/client/public/index.html) | HTML | 30 | 23 | 1 | 54 |
| [client/public/manifest.json](/client/public/manifest.json) | JSON | 25 | 0 | 1 | 26 |
| [client/src/App.css](/client/src/App.css) | CSS | 9 | 0 | 0 | 9 |
| [client/src/App.js](/client/src/App.js) | JavaScript | 49 | 0 | 9 | 58 |
| [client/src/App.test.js](/client/src/App.test.js) | JavaScript | 8 | 0 | 2 | 10 |
| [client/src/actions/auth.js](/client/src/actions/auth.js) | JavaScript | 115 | 7 | 19 | 141 |
| [client/src/actions/team.js](/client/src/actions/team.js) | JavaScript | 55 | 18 | 9 | 82 |
| [client/src/actions/types.js](/client/src/actions/types.js) | JavaScript | 11 | 0 | 2 | 13 |
| [client/src/components/Navbar.js](/client/src/components/Navbar.js) | JavaScript | 49 | 0 | 6 | 55 |
| [client/src/components/homepage/Landing.js](/client/src/components/homepage/Landing.js) | JavaScript | 59 | 0 | 3 | 62 |
| [client/src/components/homepage/Login.js](/client/src/components/homepage/Login.js) | JavaScript | 62 | 0 | 11 | 73 |
| [client/src/components/homepage/Register.js](/client/src/components/homepage/Register.js) | JavaScript | 160 | 0 | 12 | 172 |
| [client/src/components/profile/Profile.js](/client/src/components/profile/Profile.js) | JavaScript | 18 | 0 | 4 | 22 |
| [client/src/components/profile/ProfileInstructor.js](/client/src/components/profile/ProfileInstructor.js) | JavaScript | 94 | 0 | 5 | 99 |
| [client/src/components/profile/ProfileStudent.js](/client/src/components/profile/ProfileStudent.js) | JavaScript | 103 | 0 | 5 | 108 |
| [client/src/components/routing/PrivateRoute.js](/client/src/components/routing/PrivateRoute.js) | JavaScript | 25 | 0 | 5 | 30 |
| [client/src/components/subsite/AboutTab.js](/client/src/components/subsite/AboutTab.js) | JavaScript | 178 | 0 | 3 | 181 |
| [client/src/components/subsite/Brochure.js](/client/src/components/subsite/Brochure.js) | JavaScript | 50 | 0 | 4 | 54 |
| [client/src/components/subsite/HomeTab.js](/client/src/components/subsite/HomeTab.js) | JavaScript | 35 | 0 | 3 | 38 |
| [client/src/components/subsite/MediaTab.js](/client/src/components/subsite/MediaTab.js) | JavaScript | 66 | 0 | 3 | 69 |
| [client/src/components/subsite/Subsite.js](/client/src/components/subsite/Subsite.js) | JavaScript | 201 | 6 | 11 | 218 |
| [client/src/index.css](/client/src/index.css) | CSS | 12 | 0 | 2 | 14 |
| [client/src/index.js](/client/src/index.js) | JavaScript | 9 | 0 | 4 | 13 |
| [client/src/reducers/auth.js](/client/src/reducers/auth.js) | JavaScript | 57 | 0 | 3 | 60 |
| [client/src/reducers/index.js](/client/src/reducers/index.js) | JavaScript | 7 | 2 | 2 | 11 |
| [client/src/reducers/team.js](/client/src/reducers/team.js) | JavaScript | 40 | 0 | 3 | 43 |
| [client/src/setupTests.js](/client/src/setupTests.js) | JavaScript | 1 | 4 | 1 | 6 |
| [client/src/store.js](/client/src/store.js) | JavaScript | 11 | 0 | 4 | 15 |
| [client/src/utils/setAuthToken.js](/client/src/utils/setAuthToken.js) | JavaScript | 9 | 0 | 2 | 11 |
| [client/src/utils/toBase64.js](/client/src/utils/toBase64.js) | JavaScript | 6 | 0 | 0 | 6 |
| [config/db.js](/config/db.js) | JavaScript | 19 | 1 | 4 | 24 |
| [config/default.json](/config/default.json) | JSON | 6 | 0 | 0 | 6 |
| [config/production.json](/config/production.json) | JSON | 6 | 0 | 0 | 6 |
| [middleware/auth.js](/middleware/auth.js) | JavaScript | 18 | 3 | 4 | 25 |
| [models/Team.js](/models/Team.js) | JavaScript | 61 | 0 | 3 | 64 |
| [models/User.js](/models/User.js) | JavaScript | 37 | 0 | 2 | 39 |
| [package-lock.json](/package-lock.json) | JSON | 2,365 | 0 | 1 | 2,366 |
| [package.json](/package.json) | JSON | 31 | 0 | 1 | 32 |
| [server.js](/server.js) | JavaScript | 18 | 6 | 10 | 34 |

[summary](results.md)