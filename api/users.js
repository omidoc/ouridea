const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");
const randomstring = require("randomstring");
const { check, validationResult } = require("express-validator"); //to check for errors like email and password lengthi

const User = require("../models/User");
const Team = require("../models/Team");

// @route POST api/users/instructor
// @desc Register Instructor
// @access
router.post(
  "/instructor",
  [
    check("email", "please enter a valid email").isEmail(),
    check(
      "password",
      "please enter a password with min 6 characters"
    ).isLength({ min: 6 }),
    check("name", "Please enter your name").not().isEmpty(),
    check("university", "please enter the university's name").not().isEmpty(),
  ],
  async (req, res) => {
    //async function runs in a seperate order than rest of the code.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name ,email, password, university } = req.body;
    try {
      //see if user exists
      let user = await User.findOne({ email });

      if (user) {
        return res.status(400).json({
          errors: [{ msg: "User Already Exists" }],
        });
      }

      let programKey = randomstring.generate(6);
      let role = "Instructor";
      let teams = [];
      for (let i = 0; i < 5; i++) {
        let string = randomstring.generate(8);
        teams.push(string);
      }
      user = new User({
        name,
        email,
        programKey,
        university,
        password,
        role,
        teams,
      });

      //Encrypt the password
      const salt = await bcrypt.genSalt(10); //10 is the number of rounds for hashing recommended by documentatino.

      user.password = await bcrypt.hash(password, salt); // if we don't use await , we have to do it like ...then(callback) but
      //using this style is much prettier

      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"), //mysecrettoken defined in config
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("server error");
    }
  }
);

// @route POST api/users/instructor
// @desc Register Instructor
// @access
router.post(
  "/student",
  [
    check("email", "Please enter a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with min 6 characters"
    ).isLength({ min: 6 }),
    check("programKey", "Please enter the Program Key").not().isEmpty(),
    check("name", "Please enter your name").not().isEmpty(),
    check("team", "Please enter the Team ID").not().isEmpty(),
  ],
  async (req, res) => {
    //async function runs in a seperate order than rest of the code.
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password, programKey, team } = req.body;
    try {
      //see if email exists
      let user_ = await User.findOne({ email });
      if (user_) {
        return res.status(400).json({
          errors: [{ msg: "Email Already Exists" }],
        });
      }
      //see if program key is valid
      let instructor = await User.findOne({ programKey, role: "Instructor" });
      if (!instructor) {
        return res.status(400).json({
          errors: [{ msg: "Program Key is Not valid" }],
        });
      }
      let university = instructor.university;
      //see if team ID is valid
      let teams = instructor.teams;
      if (!teams.includes(team)) {
        return res.status(400).json({
          errors: [{ msg: "Team ID is not Valid" }],
        });
      }
      let role = "Student";
      student = new User({
        name,
        email,
        programKey,
        password,
        university,
        role,
        team,
      });

      //see if team exists
      let team_ = await Team.findOne({ team });
      if (!team_) {
        team_ = new Team({
          team,
          programKey,
          members: [],
        });
      }

      team_.members.unshift({ user: student.id, name: student.name });

      await team_.save();

      //Encrypt the password
      const salt = await bcrypt.genSalt(10); //10 is the number of rounds for hashing recommended by documentatino.

      student.password = await bcrypt.hash(password, salt); // if we don't use await , we have to do it like ...then(callback) but
      //using this style is much prettier

      await student.save();

      const payload = {
        user: {
          id: student.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"), //mysecrettoken defined in config
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("server error");
    }
  }
);
module.exports = router;
