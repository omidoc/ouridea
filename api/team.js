const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");
const randomstring = require("randomstring");
const { check, validationResult } = require("express-validator"); //to check for errors like email and password lengthi
const multer = require("multer");

const User = require("../models/User");
const Team = require("../models/Team");

// @route Post api/team/
// @desc Create or update a team
// @access Private
router.post("/", auth, async (req, res) => {
  let newTeam = req.body;
  try {
    let user = await User.findById(req.user.id);
    let team = await Team.findOneAndUpdate(
      { team: user.team },
      { content: newTeam.content, logo: newTeam.logo, cover: newTeam.cover },
      { new: true }
    );

    return res.json(team);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route GET api/team/
// @desc get team by team ID
// @access Private
router.get("/team/:team_id", auth, async (req, res) => {
  try {
    let team = await Team.findOne({ team: req.params.team_id });
    res.json(team);
  } catch (error) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route GET api/team/
// @desc get all the teams
// @access Private
router.get("/teams", auth, async (req, res) => {
  let user = await User.findById(req.user.id);
  if (user.role !== "Instructor") {
    return res.status(400).json({
      errors: [{ msg: "Not Reachable for non-instructors" }],
    });
  }
  try {
    const profiles = await Team.find({
      programKey: user.programKey,
    });
    res.json(profiles);
  } catch (error) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});
module.exports = router;
