const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TeamSchema = new mongoose.Schema({
  team: {
    type: String,
    required: true,
    unique: true,
  },
  programKey: {
    type: String,
    required: true,
  },
  members: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: "user",
      },
      name: {
        type: String
      }
    },
  ],
  content: {
    type: Array,
    default: [
      {
        type: "home-tab",
        heading: "Home",
        text:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis risus sit amet leo luctus, et laoreet dui cursus. Pellentesque molestie sodales nunc, in ullamcorper erat tincidunt vitae. Nullam commodo viverra elementum. Nam id blandit magna, non malesuada erat. Sed consectetur massa in nisl dapibus pharetra. Morbi maximus maximus tortor, cursus molestie massa vestibulum commodo. Sed tellus sem, posuere ut elementum pharetra, convallis at tellus. Vivamus sodales at velit quis congue. Quisque sollicitudin dolor ipsum, vel tempus augue semper at. Donec molestie lacus id tristique pellentesque. Cras sit amet elit ac nulla dictum cursus. Nam sed euismod metus.",
      },
      {
        type: "about-tab",
        heading: "About Us",
        members: [
          { lastName: "Doe", firstName: 'John', email: "example@domain.com", role: "Team Member", photo: null, about: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis risus sit amet leo luctus, et laoreet dui cursus.' },
          { lastName: "Doe", firstName: 'John', email: "example@domain.com", role: "Team Member", photo: null, about: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis risus sit amet leo luctus, et laoreet dui cursus.' },
          { lastName: "Doe", firstName: 'John', email: "example@domain.com", role: "Team Member", photo: null, about: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis risus sit amet leo luctus, et laoreet dui cursus.' },
        ],
      },
      {
        type: "media-tab",
        heading: "Media",
        mediaUrl: "https://www.youtube.com/embed/XUBeH7VQaFY",
        text:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mollis risus sit amet leo luctus, et laoreet dui cursus. Pellentesque molestie sodales nunc, in ullamcorper erat tincidunt vitae. Nullam commodo viverra elementum. Nam id blandit magna, non malesuada erat. Sed consectetur massa in nisl dapibus pharetra. Morbi maximus maximus tortor, cursus molestie massa vestibulum commodo. Sed tellus sem, posuere ut elementum pharetra, convallis at tellus. Vivamus sodales at velit quis congue. Quisque sollicitudin dolor ipsum, vel tempus augue semper at. Donec molestie lacus id tristique pellentesque. Cras sit amet elit ac nulla dictum cursus. Nam sed euismod metus.",
      },
    ],
  },
  files: {
    type: Array,
    default: [],
  },
  logo: {
    type: String,
    default: null,
  },
  cover: {
    type: String,
    default: null,
  },
});

module.exports = Team = mongoose.model("team", TeamSchema);
