const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema(
    {
        name:{
            type: String,
            required: true
        },
        email:{
            type:String,
            required: true,
            unique: true
        },
        programKey:{
            type:String,
        },
        password:{
            type: String,
            required: true
        },
        university:{
            type: String,
            required: true
        },
        date:{
            type: Date,
            default: Date.now
        },
        role:{
            type: String,
            required: true,
            enum: ['Instructor', 'Student']
        },
        teams:{
            type: Array
        },
        team:{
            type: String
        }
    }
)

module.exports = User = mongoose.model('user', UserSchema)