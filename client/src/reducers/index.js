import { combineReducers } from "redux";
// import alert from "./alert";
import auth from "./auth";
import team from "./team";

export default combineReducers({
  team,
//   alert,
  auth
});
