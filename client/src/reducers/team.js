import { UPDATE_TEAM_SUCESS, USER_LOADED, GET_TEAMS, GET_TEAM } from "../actions/types";

const initialState = {
  team: null,
  loading: true,
  teams: []
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case UPDATE_TEAM_SUCESS:
      if (payload) {
        return {
          ...state,
          loading: false,
          team: payload,
        };
      }
      break;
    case USER_LOADED:
      if (payload.team) {
        return {
          ...state,
          loading: false,
          team: payload.team,
        };
      } else return state;
    case GET_TEAMS:
      return {
        ...state,
        teams: payload,
        loading: false,
      };
    case GET_TEAM:
      return {
        ...state,
        team: payload,
        loading: false,
      };
    default:
      return state;
  }
}
