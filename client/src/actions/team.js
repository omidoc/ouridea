import axios from "axios";
import { UPDATE_TEAM_SUCESS, GET_TEAMS, TEAM_ERROR, GET_TEAM } from "./types";

//update team
export const update_team = (changedTeam) => async (dispatch) => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const res = await axios.post("/api/team", changedTeam, config);

    dispatch({
      type: UPDATE_TEAM_SUCESS,
      payload: res.data,
    });
  } catch (err) {
    const errors = err.response.data.errors;
    alert(err);
    //   dispatch({
    //     type: PROFILE_ERROR,
    //     payload: { msg: err.response.statusText, status: err.response.status }
    //   });
  }
};

export const upload_file = (file, name) => async (dispatch) => {
  try {
    // const file = e.target.files[0];
    let data = new FormData();
    data.append("file", file);
    data.append("name", name);

    let res = await axios.post("/api/team/files", data);

    dispatch({
      type: UPDATE_TEAM_SUCESS,
      payload: res.data,
    });
  } catch (err) {
    alert(err);
  }
};

//Get all teams
export const getTeam = (team_id) => async (dispatch) => {
  // dispatch({ type: CLEAR_PROFILE });
  try {
    const res = await axios.get(`/api/team/team/${team_id}`);

    dispatch({
      type: GET_TEAM,
      payload: res.data,
    });
  } catch (err) {
    alert(err);
    // dispatch({
    //   type: TEAM_ERROR,
    //   payload: { msg: err.response.statusText, status: err.response.status },
    // });
  }
};
//Get all teams
export const getTeams = () => async (dispatch) => {
  // dispatch({ type: CLEAR_PROFILE });
  try {
    const res = await axios.get("/api/team/teams");

    dispatch({
      type: GET_TEAMS,
      payload: res.data,
    });
  } catch (err) {
    alert(err);
    // dispatch({
    //   type: TEAM_ERROR,
    //   payload: { msg: err.response.statusText, status: err.response.status },
    // });
  }
};
