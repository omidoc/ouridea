export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT = "LOGOUT";
export const UPDATE_TEAM_SUCESS = "UPDATE_TEAM_SUCESS";
export const GET_TEAMS = "GET_TEAMS";
export const TEAM_ERROR = "TEAM_ERROR";
export const GET_TEAM = "GET_TEAM";

