import axios from "axios";
import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT
} from "./types";
import setAuthToken from "../utils/setAuthToken";

//Load User
export const loadUser = () => async dispatch => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }
  try {
    const res = await axios.get("/api/auth");

    return dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch (err) {
    // const errors = err.response.data.errors;

    // alert(err.message)
    return dispatch({
      type: AUTH_ERROR
    });
  }
};

//Register constructor
export const register_instructor = ({
  name,
  email,
  password,
  university
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  let role = "Instructor";
  const body = JSON.stringify({ name, email, password, university, role });

  try {
    const res = await axios.post("/api/users/instructor", body, config);

    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data
    });
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      console.log(errors);
    }

    dispatch({
      type: REGISTER_FAIL
    });
  }
};
//Register student
export const register_student = ({
  name,
  email,
  password,
  programKey,
  team
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  let role = "student";
  const body = JSON.stringify({ name, email, password, programKey, team, role });

  try {
    const res = await axios.post("/api/users/student", body, config);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data
    });
  } catch (err) {
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => alert(error.msg));
    }

    dispatch({
      type: REGISTER_FAIL
    });
  }
};

//Login User
export const login = (email, password) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({ email, password });
  try {
    const res = await axios.post("/api/auth/user", body, config);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });

    dispatch(loadUser());
  } catch (err) {
    console.log(err);
    const errors = err.response.data.errors;

    if (errors) {
      errors.forEach(error => alert(error.msg));
    }

    dispatch({
      type: LOGIN_FAIL
    });
  }
};

// Logout / Clear Profile
export const logout = history => dispatch => {
  history.push("/");
  dispatch({ type: LOGOUT });
};
