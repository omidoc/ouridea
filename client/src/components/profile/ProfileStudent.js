import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { upload_file } from "../../actions/team";

const ProfileStudent = ({ user, team, upload_file }) => {
  return (
    <div>
      <div className="uk-child-width-1-2  uk-grid">
        <div>
          <div className="uk-card uk-card-default">
            <div className="uk-card-header">
              <h3 className="uk-card-title">Information</h3>
            </div>
            <div className="uk-card-body">
              <p>Email: {user.email}</p>
              <p>Program Key: {user.programKey}</p>
              <p>Team ID: {user.team}</p>
              <p>University: {user.university}</p>
            </div>
          </div>
          <div className="uk-card uk-card-default uk-margin">
            <div className="uk-card-header">
              <h3 className="uk-card-title">Member of your Team:</h3>
            </div>
            <div className="uk-card-body">No one yet</div>
            <div className="uk-card-footer">
              <button to="./register" className="uk-button uk-button-default">
                Manage Team members
              </button>
            </div>
          </div>
        </div>
        <div>
          <div className="uk-child-width-1-1 uk-grid">
            <div>
              <div className="uk-card uk-card-default">
                <div className="uk-card-header">
                  <h3 className="uk-card-title">Our Website</h3>
                </div>
                <div className="uk-card-body">
                  <p>Description of your website</p>
                </div>
                <div className="uk-card-footer">
                  <Link
                    to={`/subsite/preview/${team.team}`}
                    className="uk-button uk-button-default"
                  >
                    Visit Website
                  </Link>
                  <Link
                    to={`/subsite/edit/${team.team}`}
                    className="uk-button uk-button-default uk-margin-left"
                  >
                    Edit Website
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="uk-child-width-1-1 uk-grid">
            <div>
              <div className="uk-card uk-card-default">
                <div className="uk-card-header">
                  <h3 className="uk-card-title">Your Files</h3>
                </div>
                <div className="uk-card-body">
                  {team.files.map((file) => (
                    <p>{file}</p>
                  ))}
                </div>
                <div className="uk-card-footer">
                  <div className="uk-margin">
                    <div className="uk-form-custom">
                      <input type="file" name="myFile" onChange={upload_file} />
                      <button
                        className="uk-button uk-button-default"
                        type="button"
                        tabIndex="-1"
                      >
                        Upload New File
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

ProfileStudent.propTypes = {
  user: PropTypes.object.isRequired,
  team: PropTypes.object,
  upload_file: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  team: state.team.team,
});

export default connect(mapStateToProps, { upload_file })(ProfileStudent);
