import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getTeams } from "../../actions/team";

const ProfileInstructor = ({ user, getTeams, teams }) => {
  useEffect(() => {
    getTeams();
  }, [getTeams]);
  return (
    <div>
      <div className="uk-child-width-1-2  uk-grid">
        <div>
          <div className="uk-card uk-card-default">
            <div className="uk-card-header">
              <h3 className="uk-card-title">Information</h3>
            </div>
            <div className="uk-card-body">
              <p>Email: {user.email}</p>
              <p>Program Key: {user.programKey}</p>
              <p>University: {user.university}</p>
            </div>
          </div>
          <div className="uk-card uk-card-default uk-margin">
            <div className="uk-card-header">
              <h3 className="uk-card-title">Active Teams</h3>
            </div>
            <div className="uk-card-body">
              <table class="uk-table uk-table-hover uk-table-divider">
                <thead>
                  <tr>
                    <th>Team ID</th>
                    <th>Team Members</th>
                  </tr>
                </thead>
                <tbody>
                  {teams.map((team) => (
                    <tr>
                      <td>{team.team}</td>
                      <td>
                        {team.members
                          .map((member) => (member.name)).join(', ')}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="uk-card-footer">
              <Link to="./brochure" className="uk-button uk-button-default">
                Show the Brochure
              </Link>
            </div>
          </div>
          <div className="uk-card uk-card-default uk-margin">
            <div className="uk-card-header">
              <h3 className="uk-card-title">Team IDs</h3>
            </div>
            <div className="uk-card-body">
              {user.teams.map((team) => (
                <p className="uk-margin" key={team}>
                  {team}{" "}
                </p>
              ))}
            </div>
            <div className="uk-card-footer">
              <Link to="./brochure" className="uk-button uk-button-default">
                Add Team
              </Link>
            </div>
          </div>
        </div>

        <div>
          <div className="uk-child-width-1-1 uk-grid">
            <div>
              <div className="uk-card uk-card-default">
                <div className="uk-card-header">
                  <h3 className="uk-card-title">Jurys</h3>
                </div>
                <div className="uk-card-body">
                  <p>Still No Jurys</p>
                </div>
                <div className="uk-card-footer">
                  <button
                    to="./register"
                    className="uk-button uk-button-default"
                  >
                    Manage Jurys
                  </button>
                </div>
              </div>
            </div>
            <div className="uk-margin">
              <div className="uk-card uk-card-default">
                <div className="uk-card-header">
                  <h3 className="uk-card-title">Professors</h3>
                </div>
                <div className="uk-card-body">
                  <p>Still No Professors</p>
                </div>
                <div className="uk-card-footer">
                  <button
                    to="./register"
                    className="uk-button uk-button-default"
                  >
                    Manage Professors
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

ProfileInstructor.propTypes = {
  user: PropTypes.object.isRequired,
  getTeams: PropTypes.func.isRequired,
  teams: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.auth.user,
  teams: state.team.teams,
});

export default connect(mapStateToProps, { getTeams })(ProfileInstructor);
