import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ProfileStudent from "./ProfileStudent";
import ProfileInstructor from "./ProfileInstructor";

const Profile = ({ user }) => {
  if (user) {
    if (user.role === "Student") return <ProfileStudent/>;
    else if (user.role === "Instructor") return <ProfileInstructor/>;
  } else return <p>Refresh the page in a few seconds..</p>;
};

Profile.propTypes = {
  user: PropTypes.object
};

const mapStateToProps = state => ({
  user: state.auth.user
});
export default connect(mapStateToProps)(Profile);
