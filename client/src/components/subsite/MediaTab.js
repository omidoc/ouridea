import React from "react";

const mediaTab = (
  heading,
  content,
  index,
  changedTeam,
  setChangedTeam,
  mediaUrl,
  mode
) => (
  <div className="uk-card uk-card-default uk-card-body uk-width-1-1@m">
    <h3
      className="uk-card-title"
      contentEditable
      suppressContentEditableWarning={true}
      onInput={(e) => {
        let newTeam = changedTeam;
        newTeam.content[index].heading = e.target.innerHTML;
        setChangedTeam(newTeam);
      }}
    >
      {heading}
    </h3>
    <p
      contentEditable
      suppressContentEditableWarning={true}
      onInput={(e) => {
        let newTeam = changedTeam;
        newTeam.content[index].text = e.target.innerHTML;
        setChangedTeam(newTeam);
      }}
    >
      {content}
    </p>
    <div className="uk-card uk-card-default uk-card-body">
      <iframe
        width="100%"
        height="300px"
        src={mediaUrl}
        title={changedTeam.team}
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
      {mode === "edit" ? (
        <button
          className="uk-button uk-button-default"
          style={{ position: "absolute", top: "40px", left: "40px", backgroundColor:'white' }}
          onClick={() => {
            var newUrl = prompt(
              "Please enter link to your new media",
              mediaUrl
            );
            let newTeam = changedTeam;
            newTeam.content[index].mediaUrl = newUrl;
            setChangedTeam(newTeam);
          }}
        >
          <i className="fas fa-edit"></i>
        </button>
      ) : (
        ""
      )}
    </div>
  </div>
);

export default mediaTab;
