import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { update_team, getTeam } from "../../actions/team";
import homeTab from "./HomeTab";
import aboutTab from "./AboutTab";
import mediaTab from "./MediaTab";
import { toBase64 } from "../../utils/toBase64";

const Subsite = ({ match, team, update_team, getTeam, loading }) => {
  const [changedTeam, setChangedTeam] = useState(team);
  const [tab, setTab] = useState(" ");
  const [loaded, setLoaded] = useState(false);
  const [content, setContent] = useState([]);
  const [htmlContent, setHtmlContent] = useState([]);
  const [finalContent, setFinalContent] = useState(null);

  const mode = match.params.mode;
  useEffect(() => {
    getTeam(match.params.team_id);
  }, [getTeam, match.params.team_id]);

  useEffect(() => {
    setChangedTeam(team)
    if (team && changedTeam) {
      setContent(team.content);
      let htmlContent_ = team.content.map((cont, index) => {
        if (cont.type === "home-tab")
          return homeTab(
            cont.heading,
            cont.text,
            index,
            changedTeam,
            setChangedTeam
          );
        else if (cont.type === "about-tab")
          return aboutTab(
            cont.heading,
            cont.members,
            index,
            changedTeam,
            setChangedTeam,
            match.params.mode
          );
        else if (cont.type === "media-tab")
          return mediaTab(
            cont.heading,
            cont.text,
            index,
            changedTeam,
            setChangedTeam,
            cont.mediaUrl,
            match.params.mode,
          );
        return cont;
      });
      setHtmlContent(htmlContent_);
    }

    setLoaded(true);
  }, [team, changedTeam, match.params.mode]);

  // console.log(content)
  // if (!team) return <p>loading</p>;
  // if (team && !changedTeam) {
  //   setTab(team.content[0].heading);
  //   setChangedTeam(team);
  // }

  if (finalContent === null && htmlContent.length > 0) {
    let finalContent_ = {};
    for (let index = 0; index < content.length; index++) {
      const key = content[index].heading;
      const value = htmlContent[index];
      finalContent_[key] = value;
    }
    setFinalContent(finalContent_);
  }

  return (
    <Fragment>
      {loading || !loaded ? (
        <p>LOADING ...</p>
      ) : (
        <Fragment>
          {!team ? (
            <p>still loading</p>
          ) : (
            <div>
              <div>
                <div
                  className={"uk-cover-container " +(mode === "edit" ? "editable-picture" : null)}
                  onClick={
                    mode === "edit"
                      ? () => document.getElementById("coverUpload").click()
                      : null
                  }
                >
                  <canvas height="200px"></canvas>
                  <img
                    src={team.cover}
                    alt=""
                    className={
                      "uk-cover " +
                      (mode === "edit" ? "editable-picture" : null)
                    }
                  />
                  {mode === "edit" ? (
                    <input
                      type="file"
                      name="cover"
                      onChange={async (e) => {
                        const file = e.target.files[0];
                        var base64String = await toBase64(file);
                        let newTeam = changedTeam;
                        newTeam.cover = base64String;
                        setChangedTeam(newTeam);
                      }}
                      id="coverUpload"
                      style={{ display: "none" }}
                    />
                  ) : (
                    ""
                  )}
                </div>
                <div
                  style={{
                    position: "absolute",
                    top: "130px",
                    overflow: 'hidden',
                    right: "15%",
                    width: "150px",
                    height: "150px",
                    borderRadius: " 75px",
                    backgroundColor: "white",
                  }}
                  className={mode === "edit" ? "editable-picture" : null}
                  onClick={
                    mode === "edit"
                      ? () => document.getElementById("logoUpload").click()
                      : null
                  }
                >
                  <img
                    src={team.logo}
                    className={"uk-cover"}
                    alt={"cover"}
                    width="150px"
                    height="150px"
                  />
                  {mode === "edit" ? (
                    <input
                      type="file"
                      name="logo"
                      onChange={async (e) => {
                        const file = e.target.files[0];
                        var base64String = await toBase64(file);
                        let newTeam = changedTeam;
                        newTeam.logo = base64String;
                        setChangedTeam(newTeam);
                      }}
                      id="logoUpload"
                      style={{ display: "none" }}
                    />
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <ul className="uk-flex-leeft uk-tab">
                <li className="uk-disabled">
                  <a href="#!">
                    <b>Company#1</b>
                  </a>
                </li>
                {content.map((tab_) => (
                  <li
                    key={tab_.heading}
                    onClick={() => setTab(tab_.heading)}
                    className={tab_.heading === tab ? "uk-active" : ""}
                  >
                    <a href="#!">{tab_.heading}</a>
                  </li>
                ))}
              </ul>
              {finalContent !== null ? <div>{finalContent[tab]}</div> : ""}
              {match.params.mode === "edit" && (
                <p className="uk-margin">
                  <button
                    className="uk-button uk-button-default"
                    onClick={() => {
                      update_team(changedTeam);
                      window.location.reload();
                    }}
                  >
                    Update
                  </button>
                </p>
              )}
            </div>
          )}
        </Fragment>
      )}
    </Fragment>
  );
};

Subsite.propTypes = {
  team: PropTypes.object,
  update_team: PropTypes.func.isRequired,
  getTeam: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  team: state.team.team,
});

export default connect(mapStateToProps, { update_team, getTeam })(Subsite);
