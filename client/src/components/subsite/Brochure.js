import React, { useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getTeams } from "../../actions/team";
import { Link } from "react-router-dom";

const Brochure = ({ getTeams, team: { teams, loading } }) => {
  useEffect(() => {
    getTeams();
  }, [getTeams]);
  return (
    <Fragment>
      {loading ? (
        <p>LOADING ...</p>
      ) : (
        <Fragment>
          <div className="uk-child-width-1-2  uk-grid">
            {teams.length > 0 ? (
              teams.map((team) => (
                <div className="uk-margin-top" key={team.team}>
                  <div className="uk-card uk-card-default  ">
                    <div className="uk-card-header">
                      <h3 className="uk-card-title">
                        {team.content[0].heading}
                      </h3>
                    </div>
                    <div className="uk-card-body ">
                      {team.content[0].text.substring(0, 200)} ...
                    </div>
                    <div className="uk-card-footer">
                      <Link
                        to={`/subsite/preview/${team.team}`}
                        className="uk-button uk-button-default"
                      >
                        Visit
                      </Link>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <p>No team found!</p>
            )}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Brochure.propTypes = {
  getTeams: PropTypes.func.isRequired,
  team: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  team: state.team,
});
export default connect(mapStateToProps, { getTeams })(Brochure);
