import React from "react";

const homeTab = (
  heading,
  content,
  index,
  changedTeam,
  setChangedTeam
) => (
  <div className="uk-card uk-card-default uk-card-body uk-width-1-1@m">
    <h3
      className="uk-card-title"
      contentEditable
      suppressContentEditableWarning={true}
      onInput={(e) => {
        let newTeam = changedTeam;
        newTeam.content[index].heading = e.target.innerHTML;
        setChangedTeam(newTeam);
      }}
    >
      {heading}
    </h3>
    <p
      contentEditable
      suppressContentEditableWarning={true}
      onInput={(e) => {
        let newTeam = changedTeam;
        newTeam.content[index].text = e.target.innerHTML;
        setChangedTeam(newTeam);
      }}
    >
      {content}
    </p>
  </div>
);

export default homeTab;
