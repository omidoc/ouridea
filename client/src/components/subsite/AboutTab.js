import React from "react";
import { toBase64 } from "../../utils/toBase64";

const aboutTab = (
  heading,
  members,
  index,
  changedTeam,
  setChangedTeam,
  mode
) => (
  <div className="uk-card uk-card-default uk-card-body uk-width-1-1@m">
    <h3
      className="uk-card-title"
      contentEditable
      suppressContentEditableWarning={true}
      onInput={(e) => {
        let newTeam = changedTeam;
        newTeam.content[index].heading = e.target.innerHTML;
        setChangedTeam(newTeam);
      }}
    >
      {heading}
    </h3>
    <div className="uk-child-width-1-3  uk-grid">
      {members.map((member, index_) => (
        <div className="uk-margin-top" key={"member" + index_}>
          <div className="uk-card uk-card-default  ">
            <div className="uk-card-header">
              <div className="uk-grid-small uk-flex-middle uk-grid">
                <div
                  className="uk-width-auto"
                  onClick={
                    mode === "edit"
                      ? () =>
                          document
                            .getElementById("profileUpload" + index_)
                            .click()
                      : null
                  }
                >
                  <img
                    alt={'Profile'}
                    className="uk-border-circle"
                    width="40"
                    height="40"
                    src={member.photo}
                  />
                  {mode === "edit" ? (
                    <input
                      type="file"
                      name="cover"
                      onChange={async (e) => {
                        const file = e.target.files[0];
                        var base64String = await toBase64(file);
                        let newTeam = changedTeam;
                        newTeam.content[index].members[
                          index_
                        ].photo = base64String;
                        setChangedTeam(newTeam);
                        console.log(newTeam);
                        alert("Press Update button to see the changes!");
                      }}
                      id={"profileUpload" + index_}
                      style={{ display: "none" }}
                    />
                  ) : (
                    ""
                  )}
                </div>
                <div className="uk-width-expand">
                  <h3
                    className="uk-card-title uk-margin-remove-bottom"
                    contentEditable={mode === "edit" ? true : false}
                    suppressContentEditableWarning={
                      mode === "edit" ? true : false
                    }
                    onInput={
                      mode === "edit"
                        ? (e) => {
                            let newTeam = changedTeam;
                            newTeam.content[index].members[index_].lastName =
                              e.target.innerHTML;
                            setChangedTeam(newTeam);
                          }
                        : null
                    }
                  >
                    {member.lastName}
                  </h3>
                  <p
                    className="uk-text-meta uk-margin-remove-top"
                    contentEditable={mode === "edit" ? true : false}
                    suppressContentEditableWarning={
                      mode === "edit" ? true : false
                    }
                    onInput={
                      mode === "edit"
                        ? (e) => {
                            let newTeam = changedTeam;
                            newTeam.content[index].members[index_].firstName =
                              e.target.innerHTML;
                            setChangedTeam(newTeam);
                          }
                        : null
                    }
                  >
                    {member.firstName}
                  </p>
                </div>
              </div>
            </div>
            <div
              className="uk-card-body"
              contentEditable={mode === "edit" ? true : false}
              suppressContentEditableWarning={mode === "edit" ? true : false}
              onInput={
                mode === "edit"
                  ? (e) => {
                      let newTeam = changedTeam;
                      newTeam.content[index].members[index_].about =
                        e.target.innerHTML;
                      setChangedTeam(newTeam);
                    }
                  : null
              }
            >
              {member.about}
            </div>
            <div className="uk-card-footer">
              <p className="uk-text-meta uk-margin-remove-top">
                Email:{" "}
                <span
                  contentEditable={mode === "edit" ? true : false}
                  suppressContentEditableWarning={
                    mode === "edit" ? true : false
                  }
                  onInput={
                    mode === "edit"
                      ? (e) => {
                          let newTeam = changedTeam;
                          newTeam.content[index].members[index_].email =
                            e.target.innerHTML;
                          setChangedTeam(newTeam);
                        }
                      : null
                  }
                >
                  {member.email}
                </span>
              </p>
              <p className="uk-text-meta uk-margin-remove-top">
                Role:{" "}
                <span
                  contentEditable={mode === "edit" ? true : false}
                  suppressContentEditableWarning={
                    mode === "edit" ? true : false
                  }
                  onInput={
                    mode === "edit"
                      ? (e) => {
                          let newTeam = changedTeam;
                          newTeam.content[index].members[index_].role =
                            e.target.innerHTML;
                          setChangedTeam(newTeam);
                        }
                      : null
                  }
                >
                  {member.role}
                </span>
              </p>
            </div>
          </div>
        </div>
      ))}
    </div>
  </div>
);

export default aboutTab;
