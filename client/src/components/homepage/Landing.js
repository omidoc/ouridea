import React from "react";
import { Link } from "react-router-dom";

const Landing = () => {
  return (
    <div className="uk-child-width-1-2@m uk-grid-match uk-grid ">
      <div>
        <div className="uk-card uk-card-default">
          <div className="uk-card-header">
            <h3 className="uk-card-title">Instructors</h3>
          </div>
          <div className="uk-card-body">
            <p>
              Here you can apply as an Instructor to our wonderful platform.
              Instructor is in the charge of starting a new course on our
              platform for their university.
            </p>
          </div>
          <div className="uk-card-footer">
            <Link to="./register/instructor" className="uk-button uk-button-default">
              Sign Up
            </Link>
            <Link
              to="./login"
              className="uk-button uk-button-default uk-margin-left"
            >
              Sign In
            </Link>
          </div>
        </div>
      </div>
      <div>
        <div className="uk-card uk-card-default">
          <div className="uk-card-header">
            <h3 className="uk-card-title">Students</h3>
          </div>
          <div className="uk-card-body">
            <p>
              Here you can apply as an Student to our wonderful platform. You
              can use our platform under supervision of your Instructor to
              introduce your idea to everyone.
            </p>
          </div>
          <div className="uk-card-footer">
          <Link to="./register/student" className="uk-button uk-button-default">
              Sign Up
            </Link>
            <Link
              to="./login"
              className="uk-button uk-button-default uk-margin-left"
            >
              Sign In
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Landing;
