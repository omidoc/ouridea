import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { register_instructor } from "../../actions/auth";
import { register_student } from "../../actions/auth";

const Register = ({
  register_instructor,
  isAuthenticated,
  match,
  register_student,
}) => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
    password2: "",
    university: "",
    team: "",
    programKey: "",
  });

  const {
    email,
    password,
    password2,
    university,
    team,
    programKey,
    name,
  } = formData;

  const onChange = (e) =>
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });

  const onSubmit = async (e) => {
    e.preventDefault();
    if (password !== password2) {
      alert("passwords do not match");
    } else {
      if (match.params.role === "instructor") {
        console.log(formData)
        register_instructor({ name, email, password, university });
        console.log("here");
      } else if (match.params.role === "student") {
        register_student({ name, email, password, programKey, team });
      }
    }
  };

  if (isAuthenticated) return <Redirect to="/profile" />;

  if (match.params.role === "instructor")
    return (
      <div className="uk-card uk-card-default uk-card-body uk-width-1-2@m uk-align-center">
        <form autoComplete="off" onSubmit={onSubmit}>
          <legend className="uk-legend uk-align-center">Register</legend>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="text"
              placeholder="Name"
              onChange={(e) => onChange(e)}
              name="name"
              value={name}
            />
          </div>

          <div className="uk-margin">
            <input
              className="uk-input"
              type="email"
              placeholder="Email"
              onChange={(e) => onChange(e)}
              name="email"
              value={email}
            />
          </div>

          <div className="uk-margin">
            <input
              className="uk-input"
              type="password"
              placeholder="Password"
              onChange={(e) => onChange(e)}
              name="password"
              value={password}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="password"
              placeholder="Repeat Password"
              onChange={(e) => onChange(e)}
              name="password2"
              value={password2}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="text"
              placeholder="University Name"
              onChange={(e) => onChange(e)}
              name="university"
              value={university}
            />
          </div>
          <div className="uk-margin">
            <button type="submit" className="uk-button uk-button-default">
              Sign Up
            </button>
          </div>
        </form>
      </div>
    );
  else if (match.params.role === "student")
    return (
      <div className="uk-card uk-card-default uk-card-body uk-width-1-2@m uk-align-center">
        <form autoComplete="off" onSubmit={onSubmit}>
          <legend className="uk-legend uk-align-center">Register</legend>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="text"
              placeholder="Name"
              onChange={(e) => onChange(e)}
              name="name"
              value={name}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="email"
              placeholder="Email"
              onChange={(e) => onChange(e)}
              name="email"
              value={email}
            />
          </div>

          <div className="uk-margin">
            <input
              className="uk-input"
              type="password"
              placeholder="Password"
              onChange={(e) => onChange(e)}
              name="password"
              value={password}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="password"
              placeholder="Repeat Password"
              onChange={(e) => onChange(e)}
              name="password2"
              value={password2}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="text"
              placeholder="Program Key"
              onChange={(e) => onChange(e)}
              name="programKey"
              value={programKey}
            />
          </div>
          <div className="uk-margin">
            <input
              className="uk-input"
              type="text"
              placeholder="Team ID"
              onChange={(e) => onChange(e)}
              name="team"
              value={team}
            />
          </div>
          <div className="uk-margin">
            <button type="submit" className="uk-button uk-button-default">
              Sign Up
            </button>
          </div>
        </form>
      </div>
    );
};

Register.propTypes = {
  register_instructor: PropTypes.func.isRequired,
  register_student: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, {
  register_instructor,
  register_student,
})(Register);
