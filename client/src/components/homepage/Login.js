import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { login } from "../../actions/auth";

const Login = ({ isAuthenticated, login }) => {
  const [formData, setFormData] = useState({
    email: "",
    password: ""
  });

  const { email, password } = formData;

  const onChange = e =>
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });

  const onSubmit = async e => {
    e.preventDefault();
    login( email, password );
  };

  if (isAuthenticated) return <Redirect to="/profile" />;

  return (
    <div className="uk-card uk-card-default uk-card-body uk-width-1-2@m uk-align-center">
      <form autoComplete="off" onSubmit={e => onSubmit(e)}>
        <legend className="uk-legend uk-align-center">Sign In</legend>
        <div className="uk-margin">
          <input
            className="uk-input"
            type="email"
            placeholder="Email"
            onChange={e => onChange(e)}
            name="email"
            value={email}
          />
        </div>

        <div className="uk-margin">
          <input
            className="uk-input"
            type="password"
            placeholder="Password"
            onChange={e => onChange(e)}
            name="password"
            value={password}
          />
        </div>
        <div className="uk-margin">
          <button type="submit" className="uk-button uk-button-default">
            Sign In
          </button>
        </div>
      </form>
    </div>
  );
};

Login.propTypes = {
  isAuthenticated: PropTypes.bool,
  login: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { login })(Login);
