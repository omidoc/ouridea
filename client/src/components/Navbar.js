import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logout } from "../actions/auth";
import { withRouter, Link } from "react-router-dom";

const Navbar = ({ isAuthenticated, logout, history }) => {
  const [expanded, setExpanded] = useState(false);
  const authLinks = (
    <Fragment>
      <li>
        <Link to="/profile">Profile</Link>
      </li>
      <li>
        <a href="#!" onClick={(e) => logout(history)}>
          Log out
        </a>
      </li>
    </Fragment>
  );

  return (
    // <div>
    //   <nav className="uk-navbar-container uk-navbar">
    //     <div className="uk-navbar-left main-logo">
    //       <ul className="uk-navbar-nav">
    //         <Link href="#!" to="/" className="uk-navbar-item uk-logo ">
    //           <img
    //           width='100px'
    //           height='100px'
    //             src={
    //               "https://upload.wikimedia.org/wikipedia/commons/9/91/Octicons-mark-github.svg"
    //             }
    //           />
    //         </Link>
    //       </ul>
    //     </div>
    //     {isAuthenticated ? authLinks : ""}
    //   </nav>
    // </div>
    <div>
      <button
        style={{
          position: "absolute",
          left: "20px",
          top: "20px",
          fontSize: "40px",
          padding: "5px 10px",
        }}
        onClick={() => setExpanded(true)}
        class="uk-button uk-button-default"
      >
        <i class="fas fa-bars"></i>
      </button>

      <ul
        class={
          "our-navbar uk-nav-default uk-card-body uk-nav uk-card" +
          (expanded ? " our-navbar-expanded" : "")
        }
      >
        <button
          onClick={() => setExpanded(false)}
          style={{
            position: "absolute",
            padding: "10px",
            width: "100%",
            bottom: "50px",
            fontSize: "40px",
          }}
          className="uk-button uk-button-default"
        >
          <i class="fas fa-arrow-alt-circle-left"></i>
        </button>

        <li>
          <img
            width="100px"
            height="100px"
            src={
              "https://upload.wikimedia.org/wikipedia/commons/9/91/Octicons-mark-github.svg"
            }
          />
        </li>
        <li class="uk-nav-divider"></li>

        <li>
          <a href="#">Menu</a>
          <ul>{isAuthenticated ? authLinks : ""}</ul>
        </li>
      </ul>
    </div>
  );
};

Navbar.propTypes = {
  isAuthenticated: PropTypes.bool,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { logout })(withRouter(Navbar));
