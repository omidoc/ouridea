import React, { Fragment, useEffect } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./App.css";

import store from "./store";
import { loadUser } from "./actions/auth";

import Landing from "./components/homepage/Landing";
import Navbar from "./components/Navbar";
import Login from "./components/homepage/Login";
import Register from "./components/homepage/Register";
import Brochure from "./components/subsite/Brochure";


import setAuthToken from "./utils/setAuthToken";
import PrivateRoute from "./components/routing/PrivateRoute";
import Profile from "./components/profile/Profile";
import Subsite from "./components/subsite/Subsite";

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser());
  },[]);
  return (
    <Provider store={store}>
      <Router>
        <div
          className="uk-section-muted uk-height-viewport"
          style={{ paddingLeft: "10%", paddingRight: "10%" }}
        >
          <Fragment>
            <Navbar />
            {/* <Route exact path="/" component={Landing} /> */}
            <Switch>
              <Route exact path="/register/:role" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/" component={Landing} />
              <PrivateRoute exact path="/brochure" component={Brochure} />
              <PrivateRoute exact path="/profile" component={Profile} />
              {/* TODO change to private route later */}
              <PrivateRoute exact path="/subsite/:mode/:team_id" component={Subsite} />
              <PrivateRoute exact path="/subsite/preview/:team_id" component={Subsite} />
            </Switch>
          </Fragment>
        </div>
      </Router>
    </Provider>
  );
};

export default App;
